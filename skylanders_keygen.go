package main

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"github.com/alecthomas/kingpin/v2"
	"os"
)

const SectorCount = 16
const KeyMask = 0xFFFFFFFFFFFF

var (
	sector = kingpin.Flag("sector", "Only print specific sector key.").Short('s').Default("-1").Int()
	hexUid = kingpin.Arg("uid", "4 byte uid in hex.").Required().String()
)

func main() {
	kingpin.Parse()

	fmt.Println("uid =", *hexUid)

	uid, err := hex.DecodeString(*hexUid)
	if err != nil {
		fmt.Println("Error decoding uid", err)
		os.Exit(1)
	}

	hash := taghash(uid)

	switch *sector {
	case -1:
		fmt.Printf("Sector 0: 4b0b20107ccb\n") //Fixed sector 0 key
		for sector := 1; sector < SectorCount; sector++ {
			key := swapAndShift(crc64ish(hash, sector))
			fmt.Printf("Sector %v: %012x\n", sector, key)
		}
	case 0:
		fmt.Printf("Sector 0: 4b0b20107ccb\n") //Fixed sector 0 key
	default:
		key := swapAndShift(crc64ish(hash, *sector))
		fmt.Printf("Sector %v: %012x\n", *sector, key)
	}
}

func swapAndShift(input uint64) uint64 {
	bytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(bytes, input)
	return binary.BigEndian.Uint64(bytes) >> 16
}

func crc64ish(result uint64, sector int) uint64 {
	const Poly = uint64(0x42f0e1eba9ea3693)
	const Top = uint64(0x800000000000)

	result ^= uint64(sector) << 40
	for i := 0; i < 8; i++ {
		if result&Top == Top {
			result = (result << 1) ^ Poly
		} else {
			result = result << 1
		}
	}
	return result
}

func taghash(uid []byte) uint64 {
	result := uint64(0x9AE903260CC4)

	for i := 0; i < len(uid); i++ {
		result = crc64ish(result, int(uid[i])) //Check this
	}

	return result
}
